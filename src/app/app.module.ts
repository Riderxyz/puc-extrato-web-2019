import { LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NebularModule } from '../nebular.module';
import { AppRoutingModule } from './app.routing';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';


import { LoginService } from './services/login.service';
import { DataService } from './services/data.service';
import { AgGridModule } from 'ag-grid-angular';
import { NbDatepickerModule } from '@nebular/theme';
import { registerLocaleData } from '@angular/common';
import localePT from '@angular/common/locales/pt';
import { HttpClientModule } from '@angular/common/http';
import { SaldoContaComponent } from './pages/saldo-conta/saldo-conta.component';
import { ProjetosComponent } from './pages/projetos/projetos.component';
import { FormsModule } from '@angular/forms';



registerLocaleData(localePT, 'pt');
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    MenuComponent,
    SaldoContaComponent,
    ProjetosComponent
  ],
  imports: [
    BrowserModule,
    NebularModule.forRoot(),
    HttpClientModule,
    FormsModule,
    NbDatepickerModule.forRoot(),
    AgGridModule.withComponents([]),
    AppRoutingModule
  ],
  providers: [
    LoginService,
    DataService,
    { provide: LOCALE_ID, useValue: 'pt' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}


