import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { UserInterface } from '../models/user.interface';
import { DataService } from './data.service';

@Injectable()
export class LoginService {
 private _userObj: UserInterface;
  public $UserObj = new Subject<UserInterface>()
  constructor(private http: HttpClient,private dataSrv: DataService) {
  }
  doLogin(userObj) {
    this.http.get(environment.url + '/api/usuarios?_usuario=' + userObj.username + '&_senha=' + userObj.password)
      .subscribe((res) => {
        this.dataSrv.userData.coordenador = JSON.parse(res[0]).tabUsuario[0].coordenador;
        this.dataSrv.userData.coordenadorNome = JSON.parse(res[0]).tabUsuario[0].nome;
        this.dataSrv.userData.coordenadorEmail = JSON.parse(res[0]).tabUsuario[0].email;
        this.$UserObj.next(this.dataSrv.userData);
      })
    return this.http.get(environment.url + '/api/usuarios?_usuario=' + userObj.username + '&_senha=' + userObj.password);
  }

}
