import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserInterface } from '../models/user.interface';
@Injectable()
export class DataService {
 private _userObj: UserInterface;

  constructor(private http: HttpClient) {

    this.userData = this.newUserObj;
   }



  inicizalizarApp() {

  }

  get userData() {
    return this._userObj;
  }

  set userData(userObj: UserInterface) {
    this._userObj = userObj;
  }

 get newUserObj() {
    const retorno: UserInterface = {
      coordenador: '',
      coordenadorNome: '',
      coordenadorEmail: '',
      dtDataInicio: new Date(),
      dtDataFinal: new Date(),
      dataInicio: '',
      dataFinal: '',

    };
    return retorno;
  }

}
