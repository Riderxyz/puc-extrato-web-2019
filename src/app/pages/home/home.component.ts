import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

directions = [
  {
  nome: 'Extrato de Projeto',
  destino: '/projeto',
  imagem: ''

},
{
  nome: 'Saldo de Projeto',
  destino: '/saldo',
  imagem: '',

},
  {
  nome: 'Total das Contas',
  destino: '/contas',
  imagem: '',

}
]


  constructor() { }

  ngOnInit() {
  }

  goTo(item) {
    console.log(item);

  }
}
