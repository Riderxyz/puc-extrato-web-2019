import { UserInterface } from './../../models/user.interface';
import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { DataService } from 'src/app/services/data.service';
import * as moment from 'moment'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  usurObj: UserInterface;
  constructor(public loginSrv: LoginService, public dataSrv: DataService) {
    console.log(moment.locale('pt-br'));
    console.log(moment().format('L'));

    this.usurObj = this.dataSrv.newUserObj;
    this.loginSrv.$UserObj.subscribe((res) => {
      this.usurObj.coordenadorNome = res.coordenadorNome;
    });
  }

  ngOnInit() {
    const usuario = {
      username: 2387,
      password: 123456
    }
    this.loginSrv.doLogin(usuario);
  }

  onDate(event) {
    console.log(moment(this.usurObj.dataInicio).format('YYYY/M/DD'));
     this.dataSrv.userData.dataInicio = moment(this.usurObj.dataInicio).format('YYYY/M/DD');
     this.dataSrv.userData.dataFinal = moment(this.usurObj.dataFinal).format('YYYY/M/DD');
    console.log(this.dataSrv.userData);
  }

}
